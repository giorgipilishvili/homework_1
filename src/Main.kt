import kotlin.math.sqrt

fun main() {
    /* Point Actions */

    val point1 = Point(2.0F, 3.0F)
    val point2 = Point(4.0F, 3.0F)

    println(point1.toString())
    println(point2.toString())

    println(point1.equals(point2))

    println(point1.movePoint())
    println(point2.movePoint())

    println("Distance between point1 and point2: ${point1.calculateDistance(point2)}")

    /* Fraction Actions */

    val f1 = Fraction(40.0, 35.0)
    val f2 = Fraction(7.0, 8.0)

    println("Addition: ${f1.addition(f2)}")
    println("Subtraction: ${f1.subtraction(f2)}")
    println("Multiply: ${f1.multiply(f2)}")
    println("Division: ${f1.division(f2)}")
    println("Reduction: ${f1.reduction()}")
}

class Point(private var x: Float, private var y: Float) {
    override fun toString(): String {
        return "$x, $y"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            if (x == other.x && y == other.y) {
                return true
            }
        }
        return false
    }

    fun movePoint(): String {
        return "${-x}, ${-y}"
    }

    fun calculateDistance(other: Any?): Any {
        if (other is Point) {
            val z = ((x - other.x) * (x - other.x)) + ((y - other.y) * (y - other.y))
            val distance = sqrt(z)
            return "$distance"
        }
        return false
    }
}

class Fraction(n: Double, d: Double) {
    private var numerator: Double = n
    private var denominator: Double = d

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator * other.denominator / denominator == other.numerator) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$numerator / $denominator"
    }

    fun reduction(): Any { // ეს ფუნქცია ბარემ მიმატება, გამოკლება, გამრავლება, გაყოფა-ზეც გამოვიყენე :)
        var a = numerator
        var b = denominator
        while (a != b) {
            if (a > b)
                a -= b
            else
                b -= a
        }
        numerator /= a
        denominator /= a
        return Point(numerator.toFloat(), denominator.toFloat())
    }

    fun addition(other: Any?): Any {
        if (other is Fraction) {
            val newNumerator = (numerator * other.denominator) + (denominator * other.numerator)
            val newDenominator = denominator * other.denominator
            return Fraction(newNumerator, newDenominator).reduction()
        }
        return false
    }

    fun subtraction(other: Any?): Any {
        if (other is Fraction) {
            val newNumerator = (numerator * other.denominator) - (denominator * other.numerator)
            val newDenominator = denominator * other.denominator
            return Fraction(newNumerator, newDenominator).reduction()
        }
        return false
    }

    fun multiply(other: Any?): Any {
        if (other is Fraction) {
            val newNumerator = numerator * other.numerator
            val newDenominator = denominator * other.denominator
            return Fraction(newNumerator, newDenominator).reduction()
        }
        return false
    }

    fun division(other: Any?): Any {
        if(other is Fraction) {
            val newNumerator = numerator * other.denominator
            val newDenominator = denominator * other.numerator
            return Fraction(newNumerator, newDenominator).reduction()
        }
        return false
    }
}